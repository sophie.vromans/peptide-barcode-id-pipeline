full_name	three_letter_code	single_letter_code	monoisotopic_mass	average_mass	include percentage
Glycine	Gly	G	57.021464	57.050000   yes 8
Alanine	Ala	A	71.037114	71.080000   yes 18
Serine	Ser	S	87.032029	87.080000   yes 6
Proline	Pro	P	97.052764	97.120000   yes 12
Valine	Val	V	99.068414	99.130000   yes 12
Threonine	Thr	T	101.047680	101.110000  yes 12
Cysteine	Cys	C	103.009190	103.140000  no
Leucine	Leu	L	113.084060	113.160000  yes 2
Isoleucine	Ile	I	113.084060	113.160000  no
Asparagine	Asn	N	114.042930	114.100000  yes 1
Aspartic_Acid	Asp	D	115.026940	115.090000  yes 11
Glutamine	Gln	Q	128.058580	128.130000  yes 1
Lysine	Lys	K	128.094960	128.170000  no
Glutamic_Acid	Glu	E	129.042590	129.120000  yes 11
Methionine	Met	M	131.040480	131.200000  no
Histidine	His	H	137.058910	137.140000  no
Phenylalanine	Phe	F	147.068410	147.180000  yes 1
Arginine	Arg	R	156.101110	156.190000  no
Tyrosine	Tyr	Y	163.063330	163.170000  yes 4
Tryptophan	Trp	W	186.079310	186.210000  yes 1
