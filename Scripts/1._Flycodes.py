#!/usr/bin/env python3
"""
Author: Sophie Vromans
Script to produce a flycode library for the tagging of codon-randomized
protein sequences.
"""
import random

def parse_codon_table(aa_table):
    """Parses the aa table into a codon bin at desired frequencies

    aa_table: list of lines, mass and freq info for one aa per line
    Returns aa_bin: list of amino acids at desired frequencies
    """
    aa_bin = []
    for line in aa_table:
        if not line.startswith('full_name'):
            line = line.split()
            if line[5] != 'no':
                single_letter = line[2]
                percentage = int((line[6]))
                for i in range(percentage):
                    aa_bin += [single_letter]
    random.shuffle(aa_bin)
    return aa_bin

def sample_X7(aa_bin):
    """Samples a 7 aa sequence from the amino acid bin, containing the amino
    acids at desired frequencies

    aa_bin: list of amino acids at desired frequencies
    Returns x7: string of seven amino acids as one letter codes
    """
    x7 = random.sample(aa_bin, k = 7)
    x7 = "".join(x7)
    return x7

def select_z04():
    """Select a Z0-4 sequence from a restricted list for the construction of
    the flycode

    Returns z04: String of 0-4 amino acids as one letter codes
    """
    z04_seqs = ['','L','QS','LTV','QEGG']
    z04 = "".join(random.sample(z04_seqs, k = 1))
    return z04

def join_pieces(x7,z04):
    """Joins the generated X7 and Z0-4 sequences, together with the invariable
    regions into a flycode

    Returns x7: string of seven amino acids as one letter codes
    Returns z04: String of 0-4 amino acids as one letter codes
    """
    flycode = 'GS' + x7 + 'W' + z04 + 'R'
    return flycode


def construct_flycode(aa_table):
    """Wrapper function for the construction of the flycode

    Returns the flycode as string representing the 11-15 amino acid sequence
    """
    # Parse the amino acids into a shuffled bin at the desired frequencies
    aa_bin = parse_codon_table(aa_table)
    # Construct random X7 part of flycode from codon bin
    x7 = sample_X7(aa_bin)
    # Select Z0-4 part of the flycode from list
    z04 = select_z04()
    # Construct flycode from the invariable- and generated sequences
    flycode = join_pieces(x7, z04)
    return flycode


def check_mz(flycode,aa_table):
    """Check whether the m/z value of the generated flycode is within the
    desired range of 550-850, assuming 99% of generated ions are doubly
    charge species

    flycode: string of 11-15 amino acids as single letter characters
    aa_table: list of lines, mass and freq info for one aa per line

    Returns mz_flag: Bool, True if calculated m/z is within desired range
    """
    mass_dict = {}
    mass_flycode = 0
    aa_flycode = list(flycode)
    #Parse amino acid table to dict as {amino acid:mass}
    for line in aa_table:
        if not line.startswith('full_name'):
            line = line.split()
            single_letter = line[2]
            av_mass = float((line[4]))
            mass_dict[single_letter] = av_mass
    #Look up mass of each amino acid in dict, add to total
    for aa in aa_flycode:
        mass_flycode += mass_dict[aa]
    #Calculate m/z of flycode, assuming doubly charged species
    mz_flycode = mass_flycode/2
    #Approve flycode if m/z if between 550 and 850
    if 550 < mz_flycode < 850:
        mz_flag = True
    else:
        mz_flag = False
    return mz_flag


def write_file(list_flycodes):
    """Writes the constructed flycodes to the flycodes_list.txt output file

    list_flycodes: list of flycodes as strings
    """
    df = open('Flycodes_out.txt', 'w')
    for code in list_flycodes:
        df.write(code)
        df.write('\n')
    df.close()


if __name__ == '__main__':
    #Open amino acid table including desired frequencies in random part flytags
    aa_table = open("amino_acids_flycode.txt").readlines()
    #Construct and check 300.000 flycodes, discard if not meeting requirements
    nr_codes = 0
    list_codes = []
    while nr_codes <5000000:
        flycode = construct_flycode(aa_table)
        #Check whether m/z is within desired range
        mz_flag = check_mz(flycode,aa_table)
        #Only add flycode to the list if not added yet
        if mz_flag == True and flycode not in list_codes:
            nr_codes += 1
            list_codes += [flycode]
            print('Nr of constructed flycodes: ' + str(nr_codes))
    #Write list of flycodes to output file
    write_file(list_codes)



