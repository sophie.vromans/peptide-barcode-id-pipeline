#!/usr/bin/env python3
"""
Author: Sophie Vromans
Script to bin the flycodes according to predicted ESP, select the best 200.000
"""
import matplotlib.pyplot as plt


def parse_input(input_lines):
    """Parses the input file containing ESP values per seqs into a list

    input_lines: list of input file lines
    Returns list_esp: list of lists as [[seq,ESP_value]]
    """
    list_esp = []
    for line in input_lines:
        line = line.strip()
        #Ignore header input file
        if line.startswith('Sequence'):
            continue
        seq, esp = line.split()
        esp = float(esp)
        esp = esp * 100
        list_esp += [[seq,esp]]
    print('parsing done')
    return list_esp

def create_bins():
    """Creates empty ESP bins to sort flycodes according to ESP value

    Returns dict_bins: dict as {(lower_bound,upper_bound):[]}
    """
    dict_bins = {}
    for index in range(0,100,1):
        lower = index
        upper = index + 1
        dict_bins[lower,upper] = []
    return dict_bins


def fill_bins(dict_bins,list_esp):
    """Fills the empty ESP bins according to predicted ESP value

    dict_bins: dict as {(lower_bound,upper_bound):[]}
    list_esp: list of lists as [[seq,ESP_value]]
    """
    # Per bin, extract lower- and upper bounds
    for one_bin in dict_bins:
        lower_bound = one_bin[0]
        upper_bound = one_bin[1]
        # Per seq, extract seq and esp, add to appropriate bin
        for index in range(len(list_esp)):
            seq, esp = list_esp[index]
            esp = float(esp)
            if lower_bound < esp <= upper_bound:
                dict_bins[one_bin] += [[seq, esp]]
    print('bins filled')
    return dict_bins

def write_flycodes_per_bin(dict_bins):
    """Writes the seqs per bin to a file, sorted by bins

    dict_bins: dict as {(lower_bound,upper_bound):[flycodes]}
    """
    output_flycodes_per_bin = open('Flycodes_per_bin.txt', 'w')
    for bin, seqs in dict_bins.items():
        bin = str(bin)
        bin = bin.replace(',',' -')
        output_flycodes_per_bin.write(bin)
        output_flycodes_per_bin.write(':\n')
        for seq in seqs:
            seq = str(seq)
            output_flycodes_per_bin.write(seq)
            output_flycodes_per_bin.write('\n')
    output_flycodes_per_bin.close()
    print('flycodes per bin written')


def select_best(list_esp):
    """Select the best 200.000 filtered flycodes according to highest ESP

    list_esp: list of lists as [[seq,ESP_value]]
    list_best: list of best seqs as strings
    """
    list_rev = []
    list_best = []
    for record in list_esp:
        seq, esp = record
        list_rev += [[esp,seq]]
    #Sort the list of flycodes according to predicted ESP values
    list_sorted = sorted(list_rev)
    #Select best 200.000 flycodes according to highest ESP values
    list_selection = list_sorted[-200000::]
    for esp, seq in list_selection:
        list_best += [[seq,esp]]
    return list_best


def write_best_flycodes(list_best):
    """Writes the best 200.000 flycodes to the output file as one seq per line

    list_best: list of best seqs as strings
    """
    outputfile_best = open('Flycodes_final.txt','w')
    for seq_esp in list_best:
        seq, esp = seq_esp
        outputfile_best.write(seq)
        outputfile_best.write('\t')
        outputfile_best.write(str(esp))
        outputfile_best.write('\n')
    outputfile_best.close()
    print('best flycodes written')


def graph_nr_per_bin(dict_bins):
    """Creates and saves a barplot displaying the nr of seqs per ESP bin

    dict_bins: dict as {(lower_bound,upper_bound):[flycodes]}
    """
    list_nrs = []
    list_uppers = []
    for bin, seqs in dict_bins.items():
        nr = len(seqs)
        list_nrs += [nr]
        lower,upper = bin
        list_uppers += [upper]
    plt.bar(height = list_nrs, x = list_uppers)
    plt.xlabel('Predicted ESP')
    plt.ylabel('Count')
    plt.title('Spread predicted ESP values among all seqs')
    plt.savefig('Flycodes_per_bin.png')
    print('graph made')



if __name__ == '__main__':
    #Open input file
    input = open('Predictions.txt', 'r').readlines()
    #Parse input into list containing seqs and predicted ESP values
    esp_per_seq = parse_input(input)
    #Create empty bins to sort seqs according to predictd ESP values
    bins = create_bins()
    #Fill bins with flycode seqs according to predicted ESP values
    bins_filled = fill_bins(bins,esp_per_seq)
    #Create a file containing all filtered sequences sorted per bin
    write_flycodes_per_bin(bins_filled)
    #Select the best 200.000 sequences from the filtered flycodes
    flycode_selection = select_best(esp_per_seq)
    #Write the selection of best 200.000 to the output file
    write_best_flycodes(flycode_selection)
    #Create barplot displaying nr of seqs per ESP bin
    graph_nr_per_bin(bins_filled)