#!/usr/bin/env python3
"""
Author: Sophie Vromans
Script to filter flycode sequences with SSRC values between 5-45
"""

def filter(lines):
    """Parses the SSRC values from the input file, filters sequences with SSRC
    between 5-45 into new list

    Returns list_filtered: list of remaining flycodes as strings
    """
    list_filtered = []
    for line in lines:
        line = line.strip()
        line = line.split(',')
        seq = line[0]
        seq = seq[1:-1]
        ssrc = line[1]
        ssrc = float(ssrc)
        if 5 < ssrc < 45:
            list_filtered += [seq]
    return list_filtered

def write_output(list_reduced):
    """Writes remaining flycodes to output file

    List reduced: list of remaining flycodes as strings
    """
    output = open('SSRC_filtered.txt','w')
    for seq in list_reduced:
        output.write(seq)
        output.write('\n')
    output.close()


if __name__ == '__main__':
    #Open input file
    input = open("Flycodes_SSRC.csv", "r").readlines()
    #Filter sequences
    list_filtered = filter(input)
    #Write remaining flycodes to output file
    write_output(list_filtered)